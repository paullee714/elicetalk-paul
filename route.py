from flask import redirect, request, render_template, jsonify, Blueprint, session, g
from models import db, User, Post
# from flask_bcrypt import Bcrypt

bp = Blueprint('bp', __name__)

# bcrypt = Bcrypt()


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('login')
    if login is None:
        g.user = None
    else:
        g.user = db.session.query(User).filter(User.id == user_id).first()


@bp.route("/", methods=['GET'])
def home():
    if not session.get('login'):
        return render_template('login.html')
    else:
        if request.method == 'GET':
            return redirect('/post')
        else:
            return jsonify("ERROR")


@bp.route("/login", methods=['GET', 'POST'])
def login():
    if session.get('login') is None:
        if request.method == 'GET':
            return render_template('login.html')
        else:
            user_id = request.form['user_id']
            user_pw = request.form['user_pw']
            user = db.session.query(User).filter(
                User.user_id == user_id).first()
            if user is not None:
                if user.user_pw == user_pw:
                    session['login'] = user.id
                    return jsonify({"result": "success"})
                else:
                    return jsonify({"result": "fail"})
            else:
                return jsonify({"result": "fail"})
    else:
        return redirect('/')


@bp.route("/join", methods=['GET', 'POST'])
def join():
    if session.get('login') is None:
        if request.method == 'GET':
            return render_template('join.html')
        else:
            user_id = request.form['user_id']
            user_pw = request.form['user_pw']
            user = User()
            user.user_id = user_id
            user.user_pw = user_pw

            db.session.add(user)
            db.session.commit()

            return jsonify({"result": "success"})
    else:
        return redirect('/')


@bp.route("/logout")
def logout():
    session['login'] = None
    return redirect('/')


@bp.route("/post", methods=["GET", "POST"])
def post():
    if request.method == 'POST':
        content = request.form['content']
        author = request.form['author']

        post = Post()
        post.author = author
        post.content = content
        db.session.add(post)
        db.session.commit()

        return jsonify({"result": "success"})
    else:
        post_list = db.session.query(Post).order_by(
            Post.created_at.desc()).all()
        return render_template('index.html', post_list=post_list)


@bp.route("/post", methods=["DELETE"])
def delete_post():
    id = request.form['id']
    author = request.form['author']

    data = db.session.query(Post).filter(
        Post.id == id, Post.author == author
    ).first()
    if data is not None:
        db.session.delete(data)
        db.session.commit()
        return jsonify({'result': 'success'})
    else:
        return jsonify({'result': 'fail'})


@bp.route("/post", methods=["PATCH"])
def update_post():
    id = request.form['id']
    content = request.form['content']
    author = db.session.query(User).filter(
        User.id == session['login']
    ).first()

    data = db.session.query(Post).filter(
        Post.id == id, Post.author == author.user_id
    ).first()

    data.content = content
    db.session.commit()

    return jsonify({'result': 'success'})
