from flask import Flask
from flask_migrate import Migrate
from models import db
from route import bp
from flask_bcrypt import Bcrypt

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://root:qwerqwer123@localhost:3306/elice"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.secret_key = 'asodfajsdofijac'
app.config['BCRYPT_LEVEL'] = 10
migrate = Migrate()

bcrypt = Bcrypt(app)
db.init_app(app)
migrate.init_app(app, db)

app.register_blueprint(bp)

if __name__ == '__main__':
    app.run(debug=True)
